function validateForm() {
    var cedula = document.getElementById('cedula').value.trim();
    var apellidos = document.getElementById('apellidos').value.trim();
    var nombres = document.getElementById('nombres').value.trim();
    var direccion = document.getElementById('direccion').value.trim();
    var telefono = document.getElementById('telefono').value.trim();
    var email = document.getElementById('email').value.trim();
  
    // Validación de cédula (exactamente 10 dígitos numéricos)
    var cedulaRegex = /^[0-9]{10}$/;
    if (!cedulaRegex.test(cedula)) {
      alert("La cédula debe contener exactamente 10 dígitos numéricos.");
      return false;
    }
  
    // Validación de teléfono (al menos 7 dígitos numéricos)
    var telefonoRegex = /^[0-9]{7,}$/;
    if (!telefonoRegex.test(telefono)) {
      alert("El teléfono debe contener al menos 7 dígitos numéricos.");
      return false;
    }
  
    // Validación de correo electrónico
    var emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    if (!emailRegex.test(email)) {
      alert("Ingrese un correo electrónico válido.");
      return false;
    }
  
    // Almacenamiento local de los datos
    var customerData = {
      cedula: cedula,
      apellidos: apellidos,
      nombres: nombres,
      direccion: direccion,
      telefono: telefono,
      email: email
    };
  
    localStorage.setItem('customerData', JSON.stringify(customerData));
    alert("Datos del cliente guardados correctamente.");
  
    // Para probar la persistencia de datos en localStorage, puedes usar console.log(localStorage.getItem('customerData'));
  
    return true;
  }
  
