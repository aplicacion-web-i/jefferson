document.addEventListener('DOMContentLoaded', function () {
    const form = document.getElementById('clienteForm');
    const mensaje = document.getElementById('mensaje');

    form.addEventListener('submit', function (event) {
        event.preventDefault();

        const nombre = form.elements['nombre'].value;
        const email = form.elements['email'].value;
        const telefono = form.elements['telefono'].value;
        const direccion =form.elements["direccion"].value;
        const cedula = form.elements["cedula"].value;
        // Aquí podrías realizar validaciones adicionales si es necesario

        const datosCliente = {
            nombre,
            email,
            telefono,
            direccion,
            cedula,
        };

        // Aquí podrías enviar los datos a un servidor o almacenarlos localmente, dependiendo de tus necesidades

        mensaje.textContent = 'Datos guardados correctamente';
        form.reset();
    });
});
