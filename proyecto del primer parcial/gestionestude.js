document.addEventListener('DOMContentLoaded', () => {
    const form = document.getElementById('studentForm');
    const studentList = document.getElementById('studentList');
    const studentDetails = document.getElementById('studentDetails');
    const deleteAllBtn = document.getElementById('deleteAllBtn');
    const deleteDetailsBtn = document.getElementById('deleteDetailsBtn');

    form.addEventListener('submit', (e) => {
        e.preventDefault();
        const firstName = document.getElementById('firstName').value;
        const lastName = document.getElementById('lastName').value;
        const age = document.getElementById('age').value;
        const medicalInfo = document.getElementById('medicalInfo').value;

        const studentItem = document.createElement('li');
        studentItem.textContent = `${firstName} ${lastName}`;

        studentItem.addEventListener('click', () => {
            showStudentDetails(firstName, lastName, age, medicalInfo);
        });

        const deleteBtn = document.createElement('button');
        deleteBtn.textContent = 'Eliminar';
        deleteBtn.classList.add('deleteBtn');
        deleteBtn.addEventListener('click', (event) => {
            event.stopPropagation(); // Evitar que se dispare el evento de mostrar detalles
            deleteStudent(studentItem);
        });

        studentItem.appendChild(deleteBtn);
        studentList.appendChild(studentItem);
        form.reset();
    });

    function showStudentDetails(firstName, lastName, age, medicalInfo) {
        studentDetails.innerHTML = `
            <p><strong>Nombre:</strong> ${firstName} ${lastName}</p>
            <p><strong>Edad:</strong> ${age}</p>
            <p><strong>Información Médica:</strong></p>
            <p>${medicalInfo}</p>
        `;
    }

    function deleteStudent(studentItem) {
        studentItem.remove();
        studentDetails.innerHTML = ''; // Limpiar detalles después de eliminar
    }

    deleteAllBtn.addEventListener('click', () => {
        studentList.innerHTML = ''; // Eliminar todos los elementos de la lista
        studentDetails.innerHTML = ''; // Limpiar detalles del estudiante
    });

    deleteDetailsBtn.addEventListener('click', () => {
        studentDetails.innerHTML = ''; // Limpiar detalles del estudiante
    });
});
