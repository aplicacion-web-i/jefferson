function submitForm() {
    // Obtener los valores de correo, contraseña y rol
    var email = document.getElementById("email").value;
    var password = document.getElementById("password").value;
    var role = document.getElementById("role").value;

    // Realizar la validación, por ejemplo, verificar que el correo y la contraseña no estén vacíos
    if (email.trim() === "" || password.trim() === "") {
        alert("Por favor, ingresa el correo y la contraseña.");
    } else {
        // Validar el formato del correo electrónico
        var emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
        if (!emailRegex.test(email)) {
            alert("Formato de correo electrónico incorrecto. Ejemplo válido: ejemplo@dominio.com");
            return; // Detener la ejecución si el formato del correo electrónico no es válido
        }

        // Validar la contraseña
        var passwordRegex = /^(?=.*[A-Z])(?=.*[\W_]).{8,}$/;
        if (!passwordRegex.test(password)) {
            alert("La contraseña debe tener al menos 8 caracteres, 1 mayúscula y 1 caracter especial.");
            return; // Detener la ejecución si la contraseña no cumple con los requisitos
        }

        // Dependiendo del rol seleccionado, redirigir a la página correspondiente
        if (role === "user") {
            // Redirigir a la página de usuario
            window.location.href = "pagina_de_usuario.html";
        } else if (role === "admin") {
            // Redirigir a la página de administrador
            window.location.href = "pagina_de_administrador.html";
        }
    }
} 
           function goToHome() {
            // Redirigir a la página de inicio
            window.location.href = "Paginaprincipal.html";
        }
