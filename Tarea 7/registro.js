// Función para redirigir al inicio
function goToHome() {
    // Redireccionar a la página de inicio
    window.location.href = "Paginaprincipal.html";
}
// Función para registrar al usuario
function registrar(event) {
event.preventDefault();
var nombre = document.getElementById("nombre").value;
var email = document.getElementById("email").value;
var password = document.getElementById("password").value;
var confirmPassword = document.getElementById("confirmPassword").value;
var userType = document.getElementById("userType").value;
var errorMessage = "";

// Validar que las contraseñas coincidan
if (nombre === "" || password === "" || confirmPassword === "" || userType === "") {
errorMessage = "Todos los campos son obligatorios";
}

// Validación de correo electrónico
var emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
if (!emailRegex.test(email)) {
errorMessage = "Formato de correo electrónico incorrecto";
}

if (password.length < 8) {
errorMessage = "La contraseña debe tener al menos 8 caracteres";
} else if (!/(?=.*[A-Z])/.test(password)) {
errorMessage = "La contraseña debe contener al menos 1 mayúscula";
} else if (!/(?=.*[\W_])/.test(password)) {
errorMessage = "La contraseña debe contener al menos 1 caracter especial";
}

if (password !== confirmPassword) {
errorMessage = "Las contraseñas no coinciden";
}

if (errorMessage !== "") {
document.getElementById("error-message").innerHTML = errorMessage;
} else {
// Aquí puedes realizar el registro del usuario o administrador
// Por ahora, simplemente redirigimos a la página de inicio de sesión

alert("¡Registrado correctamente!");
window.location.href = "iniciosesion.html";
}
}
