// Variable para mantener el índice actual de la diapositiva
let slideIndex = 0;
showSlides();

// Función para mostrar las diapositivas
function showSlides() {
     // Obtener todas las diapositivas
  const slides = document.querySelectorAll('.mySlides');
  // Ocultar todas las diapositivas
  for (let i = 0; i < slides.length; i++) {
    slides[i].style.display = 'none';
  }
  // Incrementar el índice de la diapositiva
  slideIndex++;
  // Si el índice es mayor que la cantidad de diapositivas, volver al principio
  if (slideIndex > slides.length) {
    slideIndex = 1;
  }
  // Mostrar la diapositiva actual
  slides[slideIndex - 1].style.display = 'block';
  // Llamar a la función showSlides nuevamente después de 2 segundos
  setTimeout(showSlides, 2000); // Cambia cada 2 segundos
}

